from django.apps import AppConfig


class BeasiswaDaftarpenerimaConfig(AppConfig):
    name = 'BEASISWA_daftarpenerima'
