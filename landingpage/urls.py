from django.conf.urls import url
from .views import index, login, register_mahasiswa, register_donatur, register_donatur_individual, register_donatur_yayasan

app_name = 'landingpage'

urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'^login/', login, name = 'login'),
	url(r'^register-mahasiswa/', register_mahasiswa, name = 'register-mahasiswa'),
	url(r'^register-donatur/', register_donatur, name = 'register-donatur'),	
	url(r'^register-donatur-individual', register_donatur_individual, name = 'register-donatur-individual')	,
	url(r'^register-donatur-yayasan', register_donatur_yayasan, name = 'register-donatur-yayasan')	

]
