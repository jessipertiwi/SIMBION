from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse


# Create your views here.
def index(request):
	html = loader.get_template('landingpage.html')
	context = {

	}
	return HttpResponse(html.render(context, request))

def login(request):
	html = loader.get_template('login.html')
	context = {

	}
	return HttpResponse(html.render(context, request))

def register_mahasiswa(request):
	html = loader.get_template('register-mahasiswa.html')
	context = {

	}
	return HttpResponse(html.render(context, request))

def register_donatur(request):
	html = loader.get_template('register-donatur.html')
	context = {

	}
	return HttpResponse(html.render(context, request))

def register_donatur_individual(request):
	html = loader.get_template('register-donatur-individual.html')
	context = {

	}
	return HttpResponse(html.render(context, request))

def register_donatur_yayasan(request):
	html = loader.get_template('register-donatur-yayasan.html')
	context = {

	}
	return HttpResponse(html.render(context, request))