from django.conf.urls import url
from .views import index, daftar_skema, daftar_tambah

urlpatterns = [
    url(r'^$', index, name = 'index'),
    url(r'^daftar_skema/', daftar_skema, name = 'daftar_skema'),
    url(r'^daftar_tambah/', daftar_tambah, name = 'daftar_tambah')


]