"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import landingpage.urls as landingpage
import BEASISWA_daftarskema.urls as optionskema
import BEASISWA_lihataktif.urls as lihataktif
import BEASISWA_daftarpenerima.urls as daftarpenerima
import BEASISWA_lihatpendaftar.urls as lihatpendaftar
import BEASISWA_lihatpenerima.urls as lihatpenerima

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(landingpage, namespace='landingpage')),
    url(r'^optionskema/', include(optionskema, namespace='optionskema')),
    url(r'^lihataktif/', include(lihataktif, namespace='lihataktif')),
    url(r'^daftarpenerima/', include(daftarpenerima, namespace='daftarpenerima')),    
    url(r'^lihatpendaftar/', include(lihatpendaftar, namespace='lihatpendaftar')),
    url(r'^lihatpenerima/', include(lihatpenerima, namespace='lihatpenerima')),
]
